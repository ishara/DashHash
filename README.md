*The modern alternative to QuickSFV using Rust and using [crc32fast](https://github.com/srijs/rust-crc32fast) crate.*

## What the hell is that?!
This is a dashHash. The program to validate integrity of files using the CRC32 checksum. It can be used to check a list of files against a .sfv file list, and to generate new lists.
Originally, was made as alternative for QuickSFV.

Mirror in case if repo will go down: https://git.envs.net/Adrec/DashHash

## Features
Currently, able to check files and making hashes.

## Where I can get it?!
Click on [releases](https://codeberg.org/CerdaCodes/DashHash/releases) and get an .zip archive. In archive you will have .exe file and without file extension. Without file extension is Linux version.  
Also, don't forget to use Scripts folder! They are containg Bash/Batch scripts for making and checking hashes.

## Benchmarks
The program that was used to benchmark the QuickSFV and DashHash was [hyperfine](https://github.com/sharkdp/hyperfine). Tested under Windows 10 with Ryzen 5 1600.

```
  hyperfine --warmup 3 'quicksfv.exe foo.sfv'
Benchmark #1: quicksfv.exe foo.sfv
  Time (mean ± σ):      74.2 ms ±  16.6 ms    [User: 29.7 ms, System: 24.5 ms]
  Range (min … max):    53.1 ms … 101.6 ms    50 runs
  
  hyperfine --warmup 3 'dash_hash.exe -f foo.sfv'
Benchmark #2: dash_hash.exe -f foo.sfv
  Time (mean ± σ):       4.7 ms ±   1.0 ms    [User: 2.0 ms, System: 3.5 ms]
  Range (min … max):     0.9 ms …   6.2 ms    378 runs
```

## How to build?!
Get Rust(Stable or Nightly), cd to the source code, and run in terminal or in cmd
```cargo build --release```

## License
[BSD-3 Clause](https://opensource.org/licenses/BSD-3-Clause).